//
//  CustomButton.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

struct CustomButton: View {
    var title: String
    var onTap: (() -> Void)?
    
    var body: some View {
        Button {
            // first approach
            // if onTap != nil { onTap!() }
            
            // second approach
            if let onTap { onTap() }
        } label: {
            Text(title)
                .font(.caption)
                .foregroundStyle(.white)
                .padding(.all, 16)
                .background(.black)
                .clipShape(
                    .rect(
                        topLeadingRadius: 12,
                        bottomTrailingRadius: 12
                    )
                )
        }
    }
}

#Preview {
    CustomButton(title: "Go to next screen")
}
