//
//  Animation2.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 27/10/23.
//

import SwiftUI

struct Animation2: View {
    @State private var isStarted = false
    
    var body: some View {
        VStack {
            if isStarted {
                withAnimation {
                    ZStack(alignment: .topLeading) {
                        RoundedRectangle(cornerRadius: 24)
                            .fill(.blue)
                            .ignoresSafeArea()
                        
                        .frame(height: UIScreen.main.bounds.height * 0.3)
                        
                        VStack(alignment: .leading) {
                            Text("M Ramdhan Syahputra")
                                .font(.title2)
                                .fontWeight(.bold)
                                .foregroundStyle(.white)
                                .padding(.bottom, 12)
                            
                            Text("118473900")
                                .font(.caption)
                                .underline()
                                .foregroundStyle(.white)
                        }.padding()
                    }
                    .transition(AnyTransition.opacity.animation(.easeOut))
                }
            }
            
            Spacer()
            
            Button {
                isStarted = !isStarted
            } label: {
                Text(isStarted ? "Stop animation" : "Start animation")
                    .padding(.all, 16)
                    .foregroundColor(.white)
                    .background(.blue)
                    .clipShape(RoundedRectangle(cornerRadius: 12))
        }
        }
    }
}

#Preview {
    Animation2()
}
