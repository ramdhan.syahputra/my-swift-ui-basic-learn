//
//  TabBarScreen.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 27/10/23.
//

import SwiftUI

struct TabBarScreen: View {
    @State private var isAnimated = false
    
    var body: some View {
        VStack {
            Spacer()
            
            RoundedRectangle(cornerRadius: isAnimated ? 60.0 : 25.0)
                .fill(isAnimated ? .blue : .red)
                .rotationEffect(.degrees(isAnimated ? 360 : 0))
                .offset(y: isAnimated ? 100 : 0)
                .scaleEffect(CGSize(width: isAnimated ? 0.5 : 1.0, height: isAnimated ? 0.5 : 1.0))
                .frame(width: 200, height: 200)
            
            Spacer()
            
            Button {
                withAnimation(Animation.default.repeatCount(5)) {
                    isAnimated = !isAnimated
                }
            } label: {
                Text(isAnimated ? "Stop Animation" : "Start Animation")
                    .padding(.all, 18)
                    .background(.blue)
                    .foregroundColor(.white)
                    .clipShape(RoundedRectangle(cornerRadius: 12))
            }
        }
    }
}

#Preview {
    TabBarScreen()
}
