//
//  List.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 27/10/23.
//

import SwiftUI

@Observable
class ViewModel {
    var members = [
        "Junior": ["M. Ramdhan Syahputra"],
        "Middle": ["Leo Christian", "Lakdar Arfento"],
        "Senior": ["Ghanda Syah Putra Dewa"],
        "Leader": ["Andreas Pratiwi"]
    ]
    
    func removeMember(indexSet index: IndexSet, key: String) {
        members[key]!.remove(atOffsets: index)
    }
//    var junior = ["M. Ramdhan Syahputra"]
//    var middle = ["Leo Christian", "Lakdar Arfento"]
//    var senior = ["Ghanda Syah Putra Dewa", "Andreas Pratiwi"]
}

struct ListScreen: View {
    private var viewModel = ViewModel()
    @State private var selected = "Ramdhan"
    @State private var selections = ["Ramdhan", "Lakdar"]

    var body: some View {
        NavigationStack {
            Form {
                showSection("Junior")
                showSection("Middle")
                showSection("Senior")
                showSection("Leader")
            }
            .navigationTitle("Mobile Developer")
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Picker("", selection: $selected) {
                        ForEach(selections, id: \.self) {
                            Text(String($0))
                        }
                    }
                    .pickerStyle(.segmented)
                }
                ToolbarItem {
                    EditButton()
                }
            }
        }
    }
    
    func showSection(_ key: String) -> some View {
        Section {
            ForEach(viewModel.members[key]!, id: \.self) { member in
                Text(member)
            }
            .onDelete(perform: { indexSet in
                viewModel.removeMember(indexSet: indexSet, key: key)
            })
        } header: {
            Text(key)
        }
    }
}

#Preview {
    ListScreen()
}
