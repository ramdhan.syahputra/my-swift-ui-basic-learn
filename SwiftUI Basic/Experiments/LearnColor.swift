//
//  LearnView.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 16/11/23.
//

import SwiftUI

struct LearnColor: View {
    var body: some View {
        ZStack {
            RadialGradient(colors: [Color.red, Color.blue], center: .center, startRadius: 20, endRadius: 100)

            Text("Your content")
                .foregroundStyle(.primary)
                .padding(50)
                .background(.thinMaterial)
        }
        .ignoresSafeArea()
    }
}

#Preview {
    LearnColor()
}
