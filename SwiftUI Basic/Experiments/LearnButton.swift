//
//  Button.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 16/11/23.
//

import SwiftUI

struct LearnButton: View {
    var body: some View {
        VStack(spacing: 12) {
            Button("Button", role: .cancel) {
                debugPrint("Clicked")
            }
            Button("Button", role: .destructive) {
                debugPrint("Clicked")
            }
            Button("Button") {
                debugPrint("Clicked")
            }.buttonStyle(.borderedProminent)
            Button("Button", role: .destructive) {
                debugPrint("Clicked")
            }.buttonStyle(.borderedProminent)
            Button("Button", role: .destructive) {
                debugPrint("Clicked")
            }.buttonStyle(.bordered)
            Button("Button") {
                debugPrint("Clicked")
            }.buttonStyle(.bordered)
        }
    }
}

#Preview {
    LearnButton()
}
