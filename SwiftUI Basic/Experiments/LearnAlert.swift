//
//  LearnAlert.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 16/11/23.
//

import SwiftUI

struct LearnAlert: View {
    @State private var showingAlert = false
    var body: some View {
        Button("Show alert") {
            showingAlert = true
        }.alert("Important Message", isPresented: $showingAlert) {
            Button("Delete", role: .destructive) {}
            Button("Cancel", role: .cancel) {}
        } message: {
            Text("Please read this.")
        }
    }
}

#Preview {
    LearnAlert()
}
