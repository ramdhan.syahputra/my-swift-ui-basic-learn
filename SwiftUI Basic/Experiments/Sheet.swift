//
//  Sheet.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 27/10/23.
//

import SwiftUI

struct Sheet: View {
    @State private var isSheetOpened = false
    
    var body: some View {
        ZStack {
            Button {
                isSheetOpened = !isSheetOpened
            } label: {
                Text("Open the sheet")
                    .padding(.all, 16)
                    .foregroundStyle(.white)
                    .background(.black)
                    .cornerRadius(12)
            }
            .sheet(isPresented: $isSheetOpened, content: {
                Text("Hello, World!")
                    .presentationDetents([.height(400), .height(500)]) // digunakan untuk mengatur height minimum dan maximum dari sheet itu sendiri
            })
        }
//        .blur(radius: isSheetOpened ? 3.0 : 0.0)
    }
}

#Preview {
    Sheet()
}
