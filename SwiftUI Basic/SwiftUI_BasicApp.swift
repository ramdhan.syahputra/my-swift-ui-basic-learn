//
//  SwiftUI_BasicApp.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

@main
struct SwiftUI_BasicApp: App {
    var body: some Scene {
        WindowGroup {
            MainScreen()
        }
    }
}
