//
//  FirstScreen.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

struct SecondScreen: View {
    @Binding var routes: [Routes]

    var body: some View {
        VStack {
            Image(systemName: "poweroutlet.type.f")
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)

            Text("Second Screen")
                .padding(.bottom, 50)

            CustomButton(title: "Back Screen") {
                routes.removeLast()
            }

            CustomButton(title: "Next Screen") {
                routes.append(Routes.third)
            }
        }
    }
}

#Preview {
    SecondScreen(routes: .constant([]))
}
