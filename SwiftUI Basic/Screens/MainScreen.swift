//
//  MainScreen.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

struct MainScreen: View {
    @State private var searchText = String()
    @State private var isPresented = false
    var body: some View {
        ZStack {
            if isPresented {
                withAnimation(.easeOut(duration: 1.5)) {
                    TabView {
                        NavigationManager()
                            .tabItem {
                                Label(
                                    title: { Text("Home") },
                                    icon: { Image(systemName: "house") }
                                )
                            }
                        ShopingScreen()
                            .tabItem {
                                Label(
                                    title: { Text("Shopping") },
                                    icon: { Image(systemName: "cart") }
                                )
                            }
                        SettingsScreen()
                            .tabItem {
                                Label(
                                    title: { Text("Settings") },
                                    icon: { Image(systemName: "gear") }
                                )
                            }
                            .toolbarBackground(.white, for: .tabBar)
                    }
                    .tint(.black)
                }
            } else {
                SplashScreen()
            }
        }.onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                withAnimation {
                    self.isPresented = true
                }
            }
        }
    }
}

#Preview {
    MainScreen()
}
