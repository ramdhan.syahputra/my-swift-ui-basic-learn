//
//  FirstScreen.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

struct FirstScreen: View {
    @Binding var routes: [Routes]
    
    var body: some View {
        VStack {
            Image(systemName: "house")
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)
            
            Text("Home Screen")
                .padding(.bottom, 50)
            
            CustomButton(title: "Next Screen") {
                routes.append(Routes.second)
            }
        }
    }
}

#Preview {
    FirstScreen(routes: .constant([]))
}
