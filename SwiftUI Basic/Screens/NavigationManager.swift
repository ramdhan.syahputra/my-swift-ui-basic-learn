//
//  NavigationManager.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

enum Routes {
    case first, second, third, fourth
}

struct NavigationManager: View {
    @State var routes = [Routes]()
    
    var body: some View {
        NavigationStack(path: $routes) {
            FirstScreen(routes: $routes)
                .navigationDestination(for: Routes.self) { route in
                    switch (route) {
                    case .first:
                        FirstScreen(routes: $routes)
//                            .navigationTitle("Home")
                    case .second:
                        SecondScreen(routes: $routes)
//                            .navigationTitle("Second Screen")
                    case .third:
                        ThirdScreen(routes: $routes)
//                            .navigationTitle("Third Screen")
                    case .fourth:
                        FourthScreen(routes: $routes)
//                            .navigationTitle("Fourth Screen")
                    }
                }
        }
    }
}

#Preview {
    NavigationManager()
}
