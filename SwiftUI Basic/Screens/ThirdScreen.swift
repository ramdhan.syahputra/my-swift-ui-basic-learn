//
//  FirstScreen.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

struct ThirdScreen: View {
    @Binding var routes: [Routes]
    
    var body: some View {
        VStack {
            Image(systemName: "popcorn")
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)
            
            Text("Third Screen")
                .padding(.bottom, 50)
            
            CustomButton(title: "Back Screen") {
                routes.removeLast()
            }
            
            CustomButton(title: "Next Screen") {
                routes.append(Routes.fourth)
            }
        }
    }
}

#Preview {
    ThirdScreen(routes: .constant([]))
}
