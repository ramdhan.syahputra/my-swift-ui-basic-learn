//
//  SplashScreen.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

struct SplashScreen: View {
    @State private var progress = 0.1
    
    var body: some View {
        ZStack {
            Color(.black)
                .ignoresSafeArea(.all)
            
            VStack {
                Image(systemName: "list.bullet.clipboard")
                    .resizable()
                    .scaledToFit()
                    .foregroundStyle(.white)
                    .frame(width: 200, height: 200)
                    .padding(.bottom, 12)
                
                Text("Wait a moment")
                    .font(.title3)
                    .foregroundStyle(.white)
                
                ProgressView(value: progress,
                             label: { Text("Loading...") },
                             currentValueLabel: { Text(progress.formatted(.percent.precision(.fractionLength(0)))) })
                .padding()
                .foregroundStyle(.white)
                .task {
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
                        
                        self.progress += 0.1
                        
                        if self.progress > 1.0 {
                            self.progress = 0.0
                        }
                    }
                }
                .progressViewStyle(.circular)
                .tint(.white)
            }
        }
    }
}

#Preview {
    SplashScreen()
}
