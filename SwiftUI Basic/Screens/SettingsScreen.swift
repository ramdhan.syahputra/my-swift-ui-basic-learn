//
//  SettingScreen.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

struct SettingsScreen: View {
    @State private var email = String()
    @State private var username = String()
    @State private var birthdate = Date()
    @State private var isSendNewsletter = false
    @State private var searchText = String()
    
    var body: some View {
        NavigationStack {
            Form {
                Section {
                    TextField("Email", text: $email)
                    TextField("Username", text: $username)
                    DatePicker("Birth date", selection: $birthdate, displayedComponents: .date)
                } header: {
                    Text("Personal information")
                }
                
                Section {
                    Toggle("Send newsletter", isOn: $isSendNewsletter)
                } header: {
                    Text("Action")
                }
                
                Button {
                    debugPrint("Logout")
                } label: {
                    Text("Logout")
                        .foregroundStyle(.red)
                }
            }
            .navigationTitle("Settings")
            .searchable(text: $searchText)
        }
    }
}

#Preview {
    SettingsScreen()
}
