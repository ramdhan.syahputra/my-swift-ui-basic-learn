//
//  FirstScreen.swift
//  SwiftUI Basic
//
//  Created by M Ramdhan Syahputra on 26/10/23.
//

import SwiftUI

struct FourthScreen: View {
    @Binding var routes: [Routes]
    
    var body: some View {
        VStack {
            Image(systemName: "chair.lounge")
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)
            
            Text("Fourth Screen")
                .padding(.bottom, 50)
            
            CustomButton(title: "Back Screen") {
                routes.removeLast()
            }
            
            CustomButton(title: "Back to Home") {
                routes.removeAll()
            }
        }
    }
}

#Preview {
    FourthScreen(routes: .constant([]))
}
